import json
import sys


class Response(object):

    @staticmethod
    def send(ok=False):
        json.dump({'ok': True if ok is True else False}, sys.stdout, indent=4)

    @staticmethod
    def query(nodes):
        response = []
        for node in nodes:
            response.append({
                'id': node.id,
                'parent_id': node.parent.id if node.parent is not None else '',
                'name': node.name
            })

        json.dump({'nodes': response}, sys.stdout, indent=4)
