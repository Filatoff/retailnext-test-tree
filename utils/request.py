from exception.custom import CustomException


class Request(object):

    @staticmethod
    def process(tree, action, params):
        result = None

        if action == 'add_node':
            Request.add_node(tree, params)
        elif action == 'delete_node':
            Request.delete_node(tree, params)
        elif action == 'move_node':
            Request.move_node(tree, params)
        elif action == 'query':
            result = Request.query(tree, params)
        else:
            raise CustomException('Action is not supported')

        return result

    @staticmethod
    def add_node(tree, params):
        tree.add_node(
            str(params['id']) if 'id' in params else None,
            str(params['name']) if 'name' in params else None,
            str(params['parent_id']) if 'parent_id' in params else None,
        )

    @staticmethod
    def delete_node(tree, params):
        tree.delete_node(
            str(params['id']) if 'id' in params else None
        )

    @staticmethod
    def move_node(tree, params):
        tree.move_node(
            str(params['id']) if 'id' in params else None,
            str(params['new_parent_id']) if 'new_parent_id' in params else None
        )

    @staticmethod
    def query(tree, params):
        return tree.query(
            int(params['min_depth']) if 'min_depth' in params else None,
            int(params['max_depth']) if 'max_depth' in params else None,
            params['names'] if 'names' in params and isinstance(params['names'], list) else None,
            params['ids'] if 'ids' in params and isinstance(params['ids'], list) else None,
            params['root_ids'] if 'root_ids' in params and isinstance(params['root_ids'], list) else None
        )
