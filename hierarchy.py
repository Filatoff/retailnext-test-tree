from exception.custom import CustomException
from utils.request import Request
from utils.response import Response
from core.tree import Tree
import sys
import json


tree = Tree()

while True:
    line = sys.stdin.readline()
    if not line:
        sys.exit(0)

    command = json.loads(line)
    for action, params in command.iteritems():
        try:
            result = Request.process(tree, action, params)
            if result is None:
                Response.send(ok=True)
            else:
                Response.query(result)

        except CustomException:
            Response.send(ok=False)

    sys.stdout.flush()
