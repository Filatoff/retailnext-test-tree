from exception.custom import CustomException
from node import Node


class Tree(object):

    __root = None

    def __init__(self, root=None):
        if root is not None and isinstance(root, Node):
            self.__set_root(root)

    def add_node(self, inserted_id, inserted_name, inserted_parent_id):
        inserted_id = self.__prepare_input_value(inserted_id, True, 'Node id is empty')
        inserted_name = self.__prepare_input_value(inserted_name, True, 'Node name is empty')
        inserted_parent_id = self.__prepare_input_value(inserted_parent_id, False, '')

        if inserted_parent_id == '':
            if self.__root is None:
                self.__set_root(Node(inserted_id, inserted_name, None))
            else:
                raise CustomException('Root already exists')
        else:
            if self.__has_node_by_id(inserted_id):
                raise CustomException('Node id is not unique')

            parent_node = self.__get_node_by_id(inserted_parent_id)
            if parent_node is None:
                raise CustomException('Parent node has not found')
            for sibling in parent_node.children:
                if sibling.name == inserted_name:
                    raise CustomException('Node name is not unique')

            parent_node.children.append(Node(inserted_id, inserted_name, parent_node))
            parent_node.children = sorted(parent_node.children, key=lambda child: child.name)

    def delete_node(self, deleted_id):
        deleted_id = self.__prepare_input_value(deleted_id, True, 'Node id is empty')

        deleted_node = self.__get_node_by_id(deleted_id)
        if deleted_node is None:
            raise CustomException('Node has not found')

        if len(deleted_node.children) > 0:
            raise CustomException('Node has children')

        deleted_node.parent.children.remove(deleted_node)

    def move_node(self, moved_id, new_parent_id):
        moved_id = self.__prepare_input_value(moved_id, True, 'Node id is empty')
        new_parent_id = self.__prepare_input_value(new_parent_id, True, 'New parent id is empty')

        if moved_id == new_parent_id:
            raise CustomException('Node must not be a self-parent')

        moved_node = self.__get_node_by_id(moved_id)
        if moved_node is None:
            raise CustomException('Node has not found')
        if moved_node.parent is None:
            raise CustomException('Moved node is root')

        new_parent_node = self.__get_node_by_id(new_parent_id)
        if new_parent_node is None:
            raise CustomException('New parent node has not found')

        if new_parent_id != moved_node.parent.id:
            for sibling in new_parent_node.children:
                if sibling.name == moved_node.name:
                    raise CustomException('Node name is not unique')

            if self.__has_node_by_id(new_parent_id, moved_node):
                raise CustomException('New parent must not be a descendant of moved')

            moved_node.parent.children.remove(moved_node)
            moved_node.parent = new_parent_node

            new_parent_node.children.append(moved_node)
            new_parent_node.children = sorted(new_parent_node.children, key=lambda child: child.name)

    def query(self, min_depth=None, max_depth=None, names=None, ids=None, root_ids=None):
        if root_ids is None:
            result = self.__filter_node(min_depth, max_depth, names, ids, self.__root, 0, [])
        else:
            result = []
            for root_id in root_ids:
                root_node = self.__get_node_by_id(root_id)
                if root_node is not None:
                    for node in self.__filter_node(min_depth, max_depth, names, ids, root_node, 0, []):
                        result.append(node)

        return result

    def __set_root(self, node):
        self.__root = node

    def __get_node_by_id(self, node_id, start_node=None):
        if start_node is not None:
            stack = [start_node]
        elif self.__root is not None:
            stack = [self.__root]
        else:
            return None

        while len(stack) > 0:
            current_node = stack.pop()
            if current_node.id == node_id:
                return current_node
            else:
                for children in reversed(current_node.children):
                    stack.append(children)

        return None

    def __has_node_by_id(self, node_id, start_node=None):
        return True if self.__get_node_by_id(node_id, start_node) else False

    def __filter_node(self, min_depth, max_depth, names, ids, node, depth, result):
        is_filtered = True

        if min_depth is not None and int(depth) < int(min_depth):
            is_filtered = False
        elif max_depth is not None and int(depth) > int(max_depth):
            is_filtered = False
        elif names is not None and node.name not in names:
            is_filtered = False
        elif ids is not None and node.id not in ids:
            is_filtered = False

        if is_filtered:
            result.append(node)

        for child in node.children:
            self.__filter_node(min_depth, max_depth, names, ids, child, depth+1, result)

        return result

    def __prepare_input_value(self, node_id='', check_empty=True, exc_message=''):
        node_id = str(node_id) if node_id is not None else ''
        if check_empty is True and node_id == '':
            raise CustomException(exc_message)

        return node_id
