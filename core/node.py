class Node(object):

    def __init__(self, id, name, parent):
        self.id = id
        self.name = name
        self.parent = parent
        self.children = []
